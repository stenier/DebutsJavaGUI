/***********************************************************************
 * Exemple de programme JAVA pour une saisie et un affichage graphique
 * (c)Sylvain Tenier <tenier at esigelec.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 ***********************************************************************/

package exosgui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.util.concurrent.Semaphore;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 * Fen�tre Swing pour l'affichage des exercices JAVA
 * @author Sylvain Tenier
 *
 */
public class FenetreExercice extends JFrame{
		private JPanel panneauAffichage;
		private PanneauSaisie panneauSaisie;
		private JTextArea zoneTexte;
		//synchronisation avec la  zone de saisie
		private Semaphore semaphore=new Semaphore(0);
		
		/**
		 * ajoute le contenu de la chaine � la zone de texte 
		 * @param chaine la cha�ne de caract�res � ajouter
		 */
		public void ajouterTexte(String chaine){
			if(this.zoneTexte.getText() != null && !this.zoneTexte.getText().equals("")){
				this.zoneTexte.append(chaine);
			}else{
				this.zoneTexte.setText(chaine);
			}	
		}	
		
		/**
		 * ajoute un entier � la zone de texte
		 * @param i l'entier � ajouter
		 */
		public void ajouterEntier(int i) {
			String chaineNombre=Integer.toString(i);
			this.ajouterTexte(chaineNombre);				
		}
		
		/**
		 * ajoute un r�el � la zone de texte
		 * @param d le r�el � ajouter
		 */
		public void ajouterReel(double d) {
			String chaineNombre=Double.toString(d);
			this.ajouterTexte(chaineNombre);	
		}

		public void definirTitre(String string) {
			this.setTitle(string);
		}
			
		/** Constructeur de la fen�tre
		 * Permet d'adapter la taille de la fen�tre � l'�cran
		 * @param largeur largeur de la fen�tre
		 * @param hauteur hauteur de la fen�tre
		 */
		public FenetreExercice(int largeur, int hauteur){
			//configuration de la synchronisation
			// configuration de la fen�tre ***
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.setTitle("Exercice");
			this.setSize(largeur, hauteur); //taille
			this.setLocationRelativeTo(null); //centrage
			// cr�ation des panneaux
			panneauAffichage=new JPanel();
			panneauSaisie=new PanneauSaisie(semaphore);
			// cr�ation de la zone de texte			
			this.zoneTexte=new JTextArea();		
			// param�trage du panneau de la fen�tre
			this.getContentPane().setLayout(new BorderLayout(10, 10));
			this.getContentPane().setBackground(Color.LIGHT_GRAY);
			// ajout de la zone de texte au panneau principal
			this.getContentPane().add(panneauAffichage, BorderLayout.CENTER);
			panneauAffichage.setLayout(new FlowLayout());
			panneauAffichage.add(this.zoneTexte);
			// ajout de la zone de saisie au panneau principal
			this.getContentPane().add(this.panneauSaisie, BorderLayout.LINE_END);
			// affichage de la fen�tre
			this.setVisible(true);
		}
		
		/** saisie graphique d'une chaine
		 * @param le texte � afficher pour la saisie
		 * @return la chaine saisie
		 */
		public String saisirChaine(String texteIntro){
			// mise en place de la saisie pour une chaine
			panneauSaisie.saisir(texteIntro,"chaine");
			// blocage jusqu'au clic sur le bouton
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// message pour indiquer qu'il faut mettre la valeur de retour dans une variable
			System.out.println("La valeur saisie est : "
					+panneauSaisie.getValeurSaisie()+"\nPensez � mettre le r�sultat dans une variable de type String pour l'utiliser");
			// masquage du panneau de saisie
			panneauSaisie.setVisible(false);
			// renvoi de la chaine saisie
			return panneauSaisie.getValeurSaisie();
		}
		
		/** saisie graphique d'un entier
		 * @param le texte � afficher pour la saisie 
		 * @return l'entier saisi
		 */
		public int saisirEntier(String texteIntro) {
			// mise en place de la saisie pour une chaine
			panneauSaisie.saisir(texteIntro,"entier");
			// blocage jusqu'au clic sur le bouton
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// message pour indiquer qu'il faut mettre la valeur de retour dans une variable
			System.out.println("La valeur saisie est : "
					+panneauSaisie.getValeurSaisie()+"\nPensez � mettre le r�sultat dans une variable de type int pour l'utiliser");
			// masquage du panneau de saisie
			panneauSaisie.setVisible(false);
			// renvoi de la chaine saisie
			return Integer.parseInt(panneauSaisie.getValeurSaisie());
		}
		
		/** saisie graphique d'un r�el
		 * @param le texte � afficher pour la saisie 
		 * @return l'entier saisi
		 */
		public double saisirReel(String texteIntro) {
			// mise en place de la saisie pour une chaine
			panneauSaisie.saisir(texteIntro,"reel");
			// blocage jusqu'au clic sur le bouton
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			// message pour indiquer qu'il faut mettre la valeur de retour dans une variable
			System.out.println("La valeur saisie est : "
					+panneauSaisie.getValeurSaisie()+"\nPensez � mettre le r�sultat dans une variable de type double pour l'utiliser");
			// masquage du panneau de saisie
			panneauSaisie.setVisible(false);
			// renvoi de la chaine saisie
			return Double.parseDouble(panneauSaisie.getValeurSaisie());
		}
		
		
}
