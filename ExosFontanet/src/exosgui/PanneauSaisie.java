/***********************************************************************
 * Exemple de programme JAVA pour une saisie et un affichage graphique
 * (c)Sylvain Tenier <tenier at esigelec.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 ***********************************************************************/

package exosgui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.Semaphore;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * @author Tenier
 *
 */
public class PanneauSaisie extends JPanel implements ActionListener{ 
	private String typeAttendu;
	private JLabel texteIntro;
	private JTextField saisie;
	private JButton validation;
	private String valeurSaisie;

	private Semaphore semaphore;

	/**
	 * @return the valeurSaisie
	 */
	public String getValeurSaisie() {
		return valeurSaisie;
	}

	/**
	 * Constructeur du panneau de saisie
	 * @param semaphore Permet de lib�rer le thread principal lorsque la saisie est termin�e
	 */
	public PanneauSaisie(Semaphore semaphore) {
		super();
		this.semaphore=semaphore;
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		// cr�ation des �l�ments
		this.saisie = new JTextField();
		this.texteIntro=new JLabel("Phrase d'accueil");
		this.validation=new JButton("Valider");
		// ajout des �l�ments
		this.add(texteIntro);
		this.add(saisie);
		this.add(validation);
		// ajout du Listener sur le bouton
		this.validation.addActionListener(this);
		// on masque pour n'afficher qu'en cas de besoin
		this.setVisible(false);

	}
	
	/**
	 * saisie d'une chaine, d'un entier ou d'un r�el
	 * @param phrase le texte d'introduction � afficher
	 * @param typeRetour le type attendu
	 */
	public void saisir(String phrase, String typeRetour){
		this.texteIntro.setText("<html><p>"
				+phrase
				+"</p></html>");
		this.typeAttendu=typeRetour;
		this.setVisible(true);
	}

	/**
	 * d�tection du clic sur le bouton, v�rification de la conformit� de la saisie et lib�ration du thread principal
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// on v�rifie que c'est bien le bouton valider qui a �t� press�
		if(e.getSource()==validation){
			boolean valide=false;
			valeurSaisie=saisie.getText();
			switch(this.typeAttendu){
			case "entier":
				try{
					Integer.parseInt(valeurSaisie);
					valide=true;
				}catch(Exception ex){
					System.out.println("Erreur de saisie : "+ex.getLocalizedMessage());
					this.texteIntro.setText("<html><p>Votre saisie est invalide</p>"
							+ "<p>Veuillez recommencer votre saisie avec un entier</p></html>");
				}
				break;
			case "reel":
				try{
					Double.parseDouble(valeurSaisie);
					valide=true;
				}catch(Exception ex){
					System.out.println("Erreur de saisie : "+ex.getLocalizedMessage());
					this.texteIntro.setText("<html><p>Votre saisie est invalide</p>"
							+ "<p>Veuillez recommencer votre saisie avec un r�el</p></html>");
				}
				break;
			case "chaine":
				valide=true;
				break;
			default:
				System.out.println("Type de saisie non support�, renvoi de la chaine brute ");
				valide=true;
			}
			if (valide==true){
				saisie.setText("");
				semaphore.release();
			}
		}
	}

}
