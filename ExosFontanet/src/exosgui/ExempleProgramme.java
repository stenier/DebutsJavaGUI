/***********************************************************************
 * Exemple de programme JAVA pour une saisie et un affichage graphique
 * (c)Sylvain Tenier <tenier at esigelec.fr>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 ***********************************************************************/


package exosgui;

public class ExempleProgramme {
	  public static void main(String[] args) {
		  String exempleDeSaisieDeChaine;
		  int exempleDeSaisieDEntier;
		  // code d'initialisation de la fen�tre
		  // ne pas modifier sans bonne raison
		  FenetreExercice exo = new FenetreExercice(800,400);
		  // TODO �crivez votre code en rempla�ant l'exemple suivant
		  // Voir la javadoc pour les m�thodes   
		  exo.definirTitre("Exemple d'exercice");
		  exo.ajouterTexte("Voici un exemple avec du texte.\nVoici un entier : ");
		  exo.ajouterEntier(8);
		  exo.ajouterTexte("\nJe peux �galement ajouter un r�el !!!\n");
		  exo.ajouterReel(5.3);
		  int nombreChaines=2;
		  String uneChaine="\nPour m�langer les "+nombreChaines+" je peux concat�ner";
		  exo.ajouterTexte(uneChaine);
		  exempleDeSaisieDeChaine=exo.saisirChaine("Veuillez saisir une chaine");
		  exo.ajouterTexte("\nVoici un texte saisi : " + exempleDeSaisieDeChaine);
		  exempleDeSaisieDEntier=exo.saisirEntier("Veuillez saisir un entier");
		  exo.ajouterTexte("\nVoici un entier d saisi : " + exempleDeSaisieDEntier);


	  }
}
